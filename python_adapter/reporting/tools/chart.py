# -*- coding: utf-8 -*-
'''
Created on 13.05.2016

@author: wehrmann
'''
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import datetime
import StringIO
import random
from matplotlib.dates import DateFormatter
import numpy as np
from matplotlib.ticker import FuncFormatter
import matplotlib

def autolabel(ax, rects):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')
        

def generateBarChart(data, label, value, error, title, f_value, l_value):
    width = 0.25       # the width of the bars
    opacity = 0.4
    
    labels = [x[label] for x in data[f_value:l_value]]
    ind = np.arange(len(labels))*0.3 
    fig = Figure(facecolor='white', figsize=(12, 6), dpi=150)
    
    ax = fig.add_subplot(111)
    ax.set_ylabel('Ton C')
    ax.set_title(title)
    ax.set_xticks(ind)
    ax.set_xticklabels(labels)

    stocks = [0 if x[value] is None else x[value] for x in data[f_value:l_value]]
    if error!=None:
        errors = [0 if x[error] is None else x[error] for x in data[f_value:l_value]]
        errors_abs = np.array(stocks)*np.array(errors)/100.
        rects = ax.bar(ind, stocks, width, alpha=opacity, color='r', yerr=errors_abs, facecolor='gray', edgecolor='black', ecolor='r', align='center')
    else:
        rects = ax.bar(ind, stocks, width, alpha=opacity, color='r', facecolor='gray', edgecolor='black', align='center')

    ax.set_xticklabels(ax.xaxis.get_majorticklabels(), rotation=90)

    ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    #ax.yaxis.labelpad = 20
    ax.set_ylim(ymin=0)
    #autolabel(ax, rects)

    canvas = FigureCanvas(fig)
    png_output = StringIO.StringIO()
    canvas.print_png(png_output)
    
    return png_output

def generatePieChart(data, label, value, title, f_value, l_value):
    labels = [x[label] for x in data[f_value:l_value]]
    stocks = [0 if x[value] is None else x[value] for x in data[f_value:l_value]]

    opacity=0.4
    
    fig = Figure(facecolor='white', figsize=(5,5), dpi=120)
    
    ax = fig.add_subplot(111)
    cmap = matplotlib.cm.get_cmap("prism")
    colors = cmap(np.linspace(0., 1., len(stocks)))
    pie = ax.pie(stocks, colors=colors,labels=labels,autopct='', shadow=False, startangle=4, labeldistance=1.05)
    
    for pie_wedge in pie[0]:
        pie_wedge.set_edgecolor('white')
        pie_wedge.set_alpha(0.8)
        
        
    for text in pie[1]:
        text.set_fontsize(6)
    
    canvas = FigureCanvas(fig)
    png_output = StringIO.StringIO()
    canvas.print_png(png_output)
    
    return png_output