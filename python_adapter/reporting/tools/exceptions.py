'''
Created on 12.05.2016

@author: wehrmann
'''

import datetime

class WebException(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv["status"]="<WebException>"
        rv['message'] = self.message
        rv['status_code'] = self.status_code
        rv['exception_time'] = datetime.datetime.now().isoformat()
        return rv