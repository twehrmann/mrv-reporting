
rm(list=ls())

library(nlme)
library(ggplot2)
#Esta funci�n elimina espacios
trim=function (x) gsub("^\\s+|\\s+$", "", x)

setwd("C:/redd/REDD+/Protocolos/4 PROTOCOLO NRE ESTATAL/Estimaciones/2 EstimacionesDesAgregadas/3 Degradacion/Z DegradaRaices/P_NACIONAL")

load("DegradacionRaices.RData")

#x1, x2, y1, y2, folio, vegetacion
attach(datos)

#Load fixed intercepts
fixed.intercepts=read.csv(file="InterceptsRaices.csv",header=TRUE)

Tipos=unique(as.character(datos$vegetacion))

x1.original=x1
x2.original=x2
y1.original=y1
y2.original=y2

pdf("Graphics_output.pdf")
output_csv_file="summary.csv"

#Se crean los encabezados para el archivo de salida
cat("Strata,n,",file=output_csv_file,append=FALSE)
cat("Intercept, Slope,",file=output_csv_file,append=TRUE)
cat(paste(0:8,rep(",CIinf,CIsup,",9)),file=output_csv_file,append=TRUE)
#Un "enter" para empezar a escribir en la siguiente linea
cat("\n",file=output_csv_file,append=TRUE)


for(v in Tipos)
{
    #Se imprime en la pantalla el estrato que se esta analizando
    cat("v=",v,"\n")
    
    #Se extrae el nombre del estrato
    strata=strsplit(v,split="-")[[1]][1]
    #Se eliminan los espacios
    strata=trim(strata)
    
    #pregunta si "strata" esta en "fixed.intercepts$Strata"
    if(strata%in%fixed.intercepts$Strata)
    {
            k=which(fixed.intercepts$Strata==strata)
            intercept=fixed.intercepts$Intercept[k]
                
            index=(vegetacion==v)
            x1=x1.original[index]
            y1=y1.original[index]
            x2=x2.original[index]
            y2=y2.original[index]

            n=length(x2)
            #las bases se sobreponen y a x "tiempo" se le quita el valor m�nimo
            dtfr=data.frame(id=c(1:n,1:n),x=c(x1,x2)-min(x1),y=c(y1,y2))
            dtfr$ycorrected=dtfr$y-mean(dtfr$y)
    
            model.mx <- lme(y~0+x,random=~1|id,data=dtfr)
            summary(model.mx)
            plot(intercept+predict(model.mx),dtfr$y)
    
    
            #create data.frame with new values for predictors
            #more than one predictor is possible
            new.dat <- data.frame(x=0:8)
            #predict response
            new.dat$pred <- intercept+predict(model.mx, newdata=new.dat,level=0)

            #create design matrix (para sacar los errores de las estimaciones y con)
            #esta matriz es para sacar los IC de las predicciones del modelo mixto
            Designmat <- model.matrix(eval(eval(model.mx$call$fixed)[-2]), new.dat[-ncol(new.dat)])

            #compute standard error for predictions y estimaci�n de IC
            predvar <- diag(Designmat %*% model.mx$varFix %*% t(Designmat))
            new.dat$SE <- sqrt(predvar) 
            new.dat$SE2 <- sqrt(predvar+model.mx$sigma^2)
            new.dat$lower=new.dat$pred-1.96*new.dat$SE2
            new.dat$upper=new.dat$pred+1.96*new.dat$SE2

            #Se grafican la predicci�n y las bandas
            p1 <- ggplot(new.dat,aes(x=x,y=pred)) + 
            geom_line() +
            geom_ribbon(aes(ymin=pred-1.96*SE2,ymax=pred+1.96*SE2),alpha=0.5,fill="red") +
            scale_y_continuous("y")
            plot(p1)
    
            #Se captura en el archvivo "output_csv_file" el nombre del tipo de veg y par�metros del modelo
            #cat imprime en un archivo a a la pantalla 
            slope=as.numeric(model.mx$coefficients$fixed[1])
            cat(v,file=output_csv_file,append=TRUE)
            cat(",",file=output_csv_file,append=TRUE)
            cat(n,file=output_csv_file,append=TRUE)
            cat(",",file=output_csv_file,append=TRUE)
            cat(intercept,file=output_csv_file,append=TRUE)
            cat(",",file=output_csv_file,append=TRUE)
            cat(slope,file=output_csv_file,append=TRUE)
            cat(",",file=output_csv_file,append=TRUE)
    
            #Se captura en el archvivo "output_csv_file" las predicciones e intervalos de confianza para 8 tiempos
            #Internal loop for saving predictions for each time
            for(i in 1:nrow(new.dat))
            {
                cat(new.dat$pred[i],file=output_csv_file,append=TRUE)
                cat(",",file=output_csv_file,append=TRUE)
                cat(new.dat$lower[i],file=output_csv_file,append=TRUE)
                cat(",",new.dat$upper[i],file=output_csv_file,append=TRUE)
                cat(",",file=output_csv_file,append=TRUE)
            }

            cat("\n",file=output_csv_file,append=TRUE)
        }
}
#cierra el archivo de gr�ficos
dev.off()

#Se lee la base con los FE que se acaba de crear
TablaEstDeg<-read.csv("summary.csv")

#Se estiman las incertidumbres para cada FE
TablaEstDeg$Ut1<-abs((TablaEstDeg$CIsup.1-TablaEstDeg$X1)/TablaEstDeg$X1)*100
TablaEstDeg$Ut2<-abs((TablaEstDeg$CIsup.2-TablaEstDeg$X2)/TablaEstDeg$X2)*100
TablaEstDeg$Ut3<-abs((TablaEstDeg$CIsup.3-TablaEstDeg$X3)/TablaEstDeg$X3)*100
TablaEstDeg$Ut4<-abs((TablaEstDeg$CIsup.4-TablaEstDeg$X4)/TablaEstDeg$X4)*100
TablaEstDeg$Ut5<-abs((TablaEstDeg$CIsup.5-TablaEstDeg$X5)/TablaEstDeg$X5)*100
TablaEstDeg$Ut6<-abs((TablaEstDeg$CIsup.6-TablaEstDeg$X6)/TablaEstDeg$X6)*100
TablaEstDeg$Ut7<-abs((TablaEstDeg$CIsup.7-TablaEstDeg$X7)/TablaEstDeg$X7)*100
TablaEstDeg$Ut8<-abs((TablaEstDeg$CIsup.8-TablaEstDeg$X8)/TablaEstDeg$X8)*100

#Se extrea el estrato 
TablaEstDeg$EST<-substr(x = TablaEstDeg$Strata, 
                start = 1, stop =as.integer(gregexpr("-",TablaEstDeg$Strata))-2)
TablaEstDeg$Origen<-"NACIONAL"

#Se seleccionan las columnas de inter�s
TablaEstDeg<-TablaEstDeg[,c(41,2,3,4,8,33,11,34,14,35,17,36,20,37,23,38,26,39,29,40,42)]
length(TablaEstDeg$EST)

#Se filtran los estratos con menos de 20 conglomerados
TablaEstDeg<-TablaEstDeg[TablaEstDeg$n>=20,]
length(TablaEstDeg$EST)

###A los estratos de la matriz de cambios de les imputa los FE estimados
TablaAreas<-read.csv("AreasEstratosSlV.csv")

#Se filtran las columnas
TablaAreas<-TablaAreas[,c(1,4)]

#Se filtran los estratos de bosque
TablaAreas<-TablaAreas[
TablaAreas$cves!="ACUI" &
TablaAreas$cves!="AGR_AN" &
TablaAreas$cves!="AGR_PER" &
TablaAreas$cves!="AH" &
TablaAreas$cves!="H2O" &
TablaAreas$cves!="OT" &
TablaAreas$cves!="PE",]
length(TablaAreas$cves)

#Se unen las bases "TablaEstDeg" y "TablaAreas"
TablaEstDeg<- merge(TablaAreas, TablaEstDeg, by.x = "cves", by.y = "EST",all=T)
length(TablaEstDeg$cves)

#Se guarda la tabla de estimaci�n de FE por Degradacion
write.csv(TablaEstDeg, file = "TablaFE_DegRaices.csv")
























