
###############################################################################
######################Se cargan las librer�as necesarias#######################
###############################################################################

library(doBy)
library(ggplot2)
library(grid)

###############################################################################
#############################Se carga la direcci�n#############################
###############################################################################

#Direcci�n de la cual se leer� el archivo
setwd("C:/redd/REDD+/Protocolos/4 PROTOCOLO NRE ESTATAL/Estimaciones/2 EstimacionesDesAgregadas/3 Degradacion/ManBaseNeg")

###############################################################################
##############################Se lee la base###################################
rm(list=ls(all=TRUE))

BaseT1<-read.csv("Calculo_20131030_CarbonoHectarea(2004-2012)_VERSION_20_t1.csv")
length(BaseT1$folio)

BaseT2<-read.csv("Calculo_20131030_CarbonoHectarea(2004-2012)_VERSION_20_t2.csv")
length(BaseT2$folio)

EstratoCongT1<-read.csv("sitios_serie4_5_estados_V06102015_lV.csv")
length(EstratoCongT1$NUMNAL)

EstratoCongT2<-read.csv("sitios_serie4_5_estados_V06102015_V.csv")
length(EstratoCongT2$NUMNAL)

EstratosIPCC<-read.csv("EstratosPMN_IPCC.csv")
length(EstratosIPCC$pf_redd_clave_subcat_leno_pri_sec)

AreasEstratos<-read.csv("AreasEstratos.csv")
length(AreasEstratos$Cves4_Cves5_pmn)

###############################################################################
####Se identifica la clase a la que pertenece cada estrato del INEGEI e IPCC###
#Se identifica el tipo de estrato PMN 4 por conglomerado en T1
BaseT1<- merge(BaseT1, EstratoCongT1, by.x = "folio", by.y = "NUMNAL",all=TRUE)
length(BaseT1$folio)

#Se identifica el tipo de estrato IPCC por conglomerado en T1
BaseT1<- merge(BaseT1, EstratosIPCC, by.x = "cve4_pmn", by.y = "pf_redd_clave_subcat_leno_pri_sec",all=TRUE)
length(BaseT1$folio)

#Se identifica el tipo de estrato PMN 5 por conglomerado en T2
BaseT2<- merge(BaseT2, EstratoCongT2, by.x = "folio", by.y = "NUMNAL",all=TRUE)
length(BaseT2$folio)

###############################################################################
#Se imputan los 0 en los conglomerdos reportados "Monitoreo" y que ten�an "Pradera"
BaseT1$CarbAerVivT1<-ifelse(BaseT1$tipificacion=="Monitoreo" & BaseT1$pf_redd_ipcc_2003=="Praderas",0,
#       as.numeric(as.character(BaseT1$carbono_arboles)))
        as.numeric(as.character(BaseT1$carbono_raices_por_hectarea)))
#BaseT2$CarbAerVivT2<-as.numeric(as.character(BaseT2$carbono_arboles))
BaseT2$CarbAerVivT2<-as.numeric(as.character(BaseT2$carbono_raices_por_hectarea))


###############################################################################
##############################Se unen las bases T1 y T2"#######################
Bt2t1<- merge(BaseT1, BaseT2, by.x = "folio", by.y = "folio",all=TRUE)
length(Bt2t1$folio)
#Se filtran los casos en lo que anexaron clases del IPCC no representadas
Bt2t1<-Bt2t1[!(is.na(Bt2t1$folio)),]
length(Bt2t1$folio)

###############################################################################
#Se imputan 0�s en el carbono T2 de aquellos conglomerdos en T1 tipificados "Monitoreo"
# y que ten�an "Pradera" y que en T2 tienen una tipificaci�n "Omitido-Remuestreo"
Bt2t1$CarbAerVivT2correg<-ifelse(Bt2t1$tipificacion.x=="Monitoreo" & Bt2t1$pf_redd_ipcc_2003=="Praderas"&
       Bt2t1$tipificacion.y=="Omitido-Remuestreo",0,Bt2t1$CarbAerVivT2)

###############################################################################
####Se filtran los estratos de T1 que no pertenencen a las categor�as de "Tierras####
######################## Forestales" o "Praderas" del IPCC#####################
Bt2t1=Bt2t1[Bt2t1$pf_redd_ipcc_2003=="Tierras Forestales" | Bt2t1$pf_redd_ipcc_2003=="Praderas",]
length(Bt2t1$folio)

#se filtra todas las UMP cuya "tipificaci�n" es "Inicial", "Reemplazo" o "Monitoreo" en T1
Bt2t1=Bt2t1[Bt2t1$tipificacion.x=="Inicial" |
              Bt2t1$tipificacion.x=="Reemplazo" |
              Bt2t1$tipificacion.x=="Monitoreo",]
length(Bt2t1$folio)

#se filtra todas las USM cuya "tipificaci�n" es "Inicial", "Reemplazo" o "Omitido-Remuestreo" en T2
Bt2t1=Bt2t1[Bt2t1$tipificacion.y=="Inicial" |
              Bt2t1$tipificacion.y=="Reemplazo" |
              Bt2t1$tipificacion.y=="Omitido-Remuestreo" & Bt2t1$tipificacion.x=="Monitoreo" & Bt2t1$pf_redd_ipcc_2003=="Praderas",]
length(Bt2t1$folio)

#Se filtran todos los "NA" de la variable "CarbAerViv" en T1
Bt2t1<-Bt2t1[!(is.na(Bt2t1$CarbAerVivT1)),]
length(Bt2t1$folio)
#Se filtran todos los "NA" de la variable "CarbAerVivT2correg" en T2
Bt2t1<-Bt2t1[!(is.na(Bt2t1$CarbAerVivT2correg)),]
length(Bt2t1$folio)

#write.csv(Bt2t1, file = "Bt2t1.csv")

###############################################################################
#Se concatenan los nombres abreviados de los estratos en t1  y t2
Bt2t1$EstTrnsT1T2<-paste(as.character(Bt2t1$cve4_pmn),"-",as.character(Bt2t1$cve5_pmn))

#se filtra todas las USP que permanecieron en el mismo estrato entre T1 y T2
Bt2t1=Bt2t1[
Bt2t1$EstTrnsT1T2=="ACUI - ACUI"  |
Bt2t1$EstTrnsT1T2=="AGR - AGR"  |
Bt2t1$EstTrnsT1T2=="AH - AH"  |
Bt2t1$EstTrnsT1T2=="BC - BC"  |
Bt2t1$EstTrnsT1T2=="BCO/P - BCO/P"  |
Bt2t1$EstTrnsT1T2=="BCO/S - BCO/S"  |
Bt2t1$EstTrnsT1T2=="BE/P - BE/P"  |
Bt2t1$EstTrnsT1T2=="BE/S - BE/S"  |
Bt2t1$EstTrnsT1T2=="BM/P - BM/P"  |
Bt2t1$EstTrnsT1T2=="BM/S - BM/S"  |
Bt2t1$EstTrnsT1T2=="EOTL/P - EOTL/P"  |
Bt2t1$EstTrnsT1T2=="EOTL/S - EOTL/S"  |
Bt2t1$EstTrnsT1T2=="EOTnL/P - EOTnL/P"  |
Bt2t1$EstTrnsT1T2=="H2O - H2O"  |
Bt2t1$EstTrnsT1T2=="MXL/P - MXL/P"  |
Bt2t1$EstTrnsT1T2=="MXL/S - MXL/S"  |
Bt2t1$EstTrnsT1T2=="MXnL/P - MXnL/P"  |
Bt2t1$EstTrnsT1T2=="MXnL/S - MXnL/S"  |
Bt2t1$EstTrnsT1T2=="OT - OT"  |
Bt2t1$EstTrnsT1T2=="P - P"  |
Bt2t1$EstTrnsT1T2=="SC/P - SC/P"  |
Bt2t1$EstTrnsT1T2=="SC/S - SC/S"  |
Bt2t1$EstTrnsT1T2=="SP/P - SP/P"  |
Bt2t1$EstTrnsT1T2=="SP/S - SP/S"  |
Bt2t1$EstTrnsT1T2=="SSC/P - SSC/P"  |
Bt2t1$EstTrnsT1T2=="SSC/S - SSC/S"  |
Bt2t1$EstTrnsT1T2=="VHL/P - VHL/P"  |
Bt2t1$EstTrnsT1T2=="VHL/S - VHL/S"  |
Bt2t1$EstTrnsT1T2=="VHnL/P - VHnL/P",]
length(Bt2t1$folio)

#Se anualiza la variable cambio de carbono
#Se crea la variable decimal de fecha en t1
Bt2t1$FechaDecimalT1<-(as.numeric(substr(x = Bt2t1$levantamiento_fecha_ejecucion.x, start = 1, stop = 2))+
                as.numeric(substr(x = Bt2t1$levantamiento_fecha_ejecucion.x, start = 4, stop = 5))*30)/365+
                as.numeric(substr(x = Bt2t1$levantamiento_fecha_ejecucion.x, start = 7, stop = 10))
Bt2t1$FechaDecimalT2<-(as.numeric(substr(x = Bt2t1$levantamiento_fecha_ejecucion.y, start = 1, stop = 2))+
                as.numeric(substr(x = Bt2t1$levantamiento_fecha_ejecucion.y, start = 4, stop = 5))*30)/365+
                as.numeric(substr(x = Bt2t1$levantamiento_fecha_ejecucion.y, start = 7, stop = 10))
Bt2t1$DifTiempoT2T1p<-(Bt2t1$FechaDecimalT2-Bt2t1$FechaDecimalT1)

#Se imputa una diferencia de medic�n de 5 a�os en las UMP de "Monitoreo"
Bt2t1$NA_FechaDecimalT1<-ifelse(is.na(Bt2t1$DifTiempoT2T1), 1,0)
Bt2t1$DifTiempoT2T1<-ifelse(Bt2t1$NA_FechaDecimalT1==1,5,Bt2t1$DifTiempoT2T1p)

#Se crea la variable de cambio de carbono/HA/a�o######################################
#Se estima el carbono en ton/ha en T1
Bt2t1$CarbAereoHaT1<-as.numeric(as.character(Bt2t1$CarbAerVivT1))
#Se estima el carbono en ton/ha en T2
Bt2t1$CarbAereoHaT2<-as.numeric(as.character(Bt2t1$CarbAerVivT2))

#Se calcula el cambio de carbono bruto entre T2 t T1
Bt2t1$CCHa<-Bt2t1$CarbAereoHaT2-Bt2t1$CarbAereoHaT1
#Se anualiza el cambio de carbono entre T2-T1
Bt2t1$CCanualizadoHa<-Bt2t1$CCHa/Bt2t1$DifTiempoT2T1
length(Bt2t1$folio)

#*****************************************************************************#
#A)FACTORES DE ABSORCI�N-ZONAS DE PERDIDA#################################

#Se crea una base en la que se filtran los cambios positivos 
Bt2t1Neg=Bt2t1[Bt2t1$CCanualizadoHa<0,]
length(Bt2t1Neg$folio)
Bt2t1Neg=Bt2t1Neg[!is.na(Bt2t1Neg$CCanualizadoHa),]
length(Bt2t1Neg$folio)
#write.csv(Bt2t1Neg, file = "Bt2t1NegRaices.csv")

#Se crea un estrato "EstratoInegei"-"Tiempo de remedici�n"
Bt2t1Neg$EstTrnsT1T2tiemRem<-paste(Bt2t1Neg$EstTrnsT1T2,"--",Bt2t1Neg$TiempoRem)

#Se filtran las UMP del estado de Campeche
Bt2t1NegCam<-Bt2t1Neg[Bt2t1Neg$estado.x=="Campeche",]
length(Bt2t1NegCam$folio)

Bt2t1NegYuc<-Bt2t1Neg[Bt2t1Neg$estado.x=="Yucatan",]
length(Bt2t1NegYuc$folio)

Bt2t1NegQR<-Bt2t1Neg[Bt2t1Neg$estado.x=="Quintana Roo",]
length(Bt2t1NegQR$folio)

Bt2t1NegChi<-Bt2t1Neg[Bt2t1Neg$estado.x=="Chiapas",]
length(Bt2t1NegChi$folio)

Bt2t1NegJal<-Bt2t1Neg[Bt2t1Neg$estado.x=="Jalisco",]
length(Bt2t1NegJal$folio)

Bt2t1NegOax<-Bt2t1Neg[Bt2t1Neg$estado.x=="Oaxaca",]
length(Bt2t1NegOax$folio)

Bt2t1NegTab<-Bt2t1Neg[Bt2t1Neg$estado.x=="Tabasco",]
length(Bt2t1NegTab$folio)

################################################################################
#Se crea el archivo de "Recuperacion" por estado#
################################################################################
#NACIONAL
y1=Bt2t1Neg$CarbAereoHaT1
y2=Bt2t1Neg$CarbAereoHaT2
x1=Bt2t1Neg$FechaDecimalT1
x2=Bt2t1Neg$FechaDecimalT2
folio=Bt2t1Neg$folio
estado=Bt2t1Neg$estado.x
vegetacion=as.character(Bt2t1Neg$EstTrnsT1T2)
datos=data.frame(y1,y2,x1,x2,folio,vegetacion,estado)
length(datos$folio)
save(file="DegradacionRaices.RData",datos)

#Campeche
y1=Bt2t1NegCam$CarbAereoHaT1
y2=Bt2t1NegCam$CarbAereoHaT2
x1=Bt2t1NegCam$FechaDecimalT1
x2=Bt2t1NegCam$FechaDecimalT2
folio=Bt2t1NegCam$folio
estado=Bt2t1NegCam$estado.x
vegetacion=as.character(Bt2t1NegCam$EstTrnsT1T2)
datos=data.frame(y1,y2,x1,x2,folio,vegetacion,estado)
length(datos$folio)
save(file="DegradacionRaicesCam.RData",datos)

#Yucat�n
y1=Bt2t1NegYuc$CarbAereoHaT1
y2=Bt2t1NegYuc$CarbAereoHaT2
x1=Bt2t1NegYuc$FechaDecimalT1
x2=Bt2t1NegYuc$FechaDecimalT2
folio=Bt2t1NegYuc$folio
estado=Bt2t1NegYuc$estado.x
vegetacion=as.character(Bt2t1NegYuc$EstTrnsT1T2)
datos=data.frame(y1,y2,x1,x2,folio,vegetacion,estado)
length(datos$folio)
save(file="DegradacionRaicesYuc.RData",datos)

#Quintana Roo
y1=Bt2t1NegQR$CarbAereoHaT1
y2=Bt2t1NegQR$CarbAereoHaT2
x1=Bt2t1NegQR$FechaDecimalT1
x2=Bt2t1NegQR$FechaDecimalT2
folio=Bt2t1NegQR$folio
estado=Bt2t1NegQR$estado.x
vegetacion=as.character(Bt2t1NegQR$EstTrnsT1T2)
datos=data.frame(y1,y2,x1,x2,folio,vegetacion,estado)
length(datos$folio)
save(file="DegradacionRaicesQR.RData",datos)

#Chiapas
y1=Bt2t1NegChi$CarbAereoHaT1
y2=Bt2t1NegChi$CarbAereoHaT2
x1=Bt2t1NegChi$FechaDecimalT1
x2=Bt2t1NegChi$FechaDecimalT2
folio=Bt2t1NegChi$folio
estado=Bt2t1NegChi$estado.x
vegetacion=as.character(Bt2t1NegChi$EstTrnsT1T2)
datos=data.frame(y1,y2,x1,x2,folio,vegetacion,estado)
length(datos$folio)
save(file="DegradacionRaicesChi.RData",datos)

#Jalisco
y1=Bt2t1NegJal$CarbAereoHaT1
y2=Bt2t1NegJal$CarbAereoHaT2
x1=Bt2t1NegJal$FechaDecimalT1
x2=Bt2t1NegJal$FechaDecimalT2
folio=Bt2t1NegJal$folio
estado=Bt2t1NegJal$estado.x
vegetacion=as.character(Bt2t1NegJal$EstTrnsT1T2)
datos=data.frame(y1,y2,x1,x2,folio,vegetacion,estado)
length(datos$folio)
save(file="DegradacionRaicesJal.RData",datos)

#Oaxaca
y1=Bt2t1NegOax$CarbAereoHaT1
y2=Bt2t1NegOax$CarbAereoHaT2
x1=Bt2t1NegOax$FechaDecimalT1
x2=Bt2t1NegOax$FechaDecimalT2
folio=Bt2t1NegOax$folio
estado=Bt2t1NegOax$estado.x
vegetacion=as.character(Bt2t1NegOax$EstTrnsT1T2)
datos=data.frame(y1,y2,x1,x2,folio,vegetacion,estado)
length(datos$folio)
save(file="DegradacionRaicesOax.RData",datos)

#Tabasco
y1=Bt2t1NegTab$CarbAereoHaT1
y2=Bt2t1NegTab$CarbAereoHaT2
x1=Bt2t1NegTab$FechaDecimalT1
x2=Bt2t1NegTab$FechaDecimalT2
folio=Bt2t1NegTab$folio
estado=Bt2t1NegTab$estado.x
vegetacion=as.character(Bt2t1NegTab$EstTrnsT1T2)
datos=data.frame(y1,y2,x1,x2,folio,vegetacion,estado)
length(datos$folio)
save(file="DegradacionRaicesTab.RData",datos)


